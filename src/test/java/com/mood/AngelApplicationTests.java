package com.mood;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import com.mood.notify.DingDingNotifyAT;
import com.mood.notify.DingDingNotifyMarkdown;
import com.mood.notify.DingDingNotifyMessageMarkdown;
import com.mood.utils.HttpClientUtil;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AngelApplicationTests {

	@Value("${dingding.webhookUrl.monitor_dingding_broadcast}")
	private String path;
	@Test
	public void testHttpPost() throws JsonProcessingException {

		HttpClientUtil httpClientUtil=HttpClientUtil.getInstance();
        DingDingNotifyMessageMarkdown dingDingNotifyMessageMarkdown=new DingDingNotifyMessageMarkdown();
        dingDingNotifyMessageMarkdown.setMsgtype(DingDingNotifyMessageMarkdown.type);
        StringBuffer sb=new StringBuffer();
		sb.append(dingDingNotifyMessageMarkdown.createTitle("李光小老弟",4))
		.append(dingDingNotifyMessageMarkdown.createReference(dingDingNotifyMessageMarkdown.createItalics("在HotSpot虚拟机中java对象的内存布局一般为对象头（mark word）,data,padding，如果是数组对象那么还有一个length属性." +
				"\n Mark Word 一般存放两部分的信息，其中一个为Class Point 一个指针，jvm虚拟机通过该指针确定该对象具体是那个类的实例。另一部分存放信息比较繁杂，比如：哈希码,GC年龄，还有锁的状态标志，线程持有的锁，偏向线程的ID，偏向时间戳，在32位和64位的jvm虚拟机(未开启压缩指针)中分别为32bit和64bit。Mark Word的数据结构不是固定的，可以根据对象的状态复用自己的存储空间。以32bit的虚拟机为例结构如下：对象处于未锁定状态：25bit存储对象的哈希码，4bit存GC年龄，2bit存放锁的标志位，1bit固定为0；" +
				"\n对其填充，就是占位符的作用，用来避免cpu的缓存行导致的伪共享。")))
		.append(dingDingNotifyMessageMarkdown.createImageOrLink("更多","http://moodfly.top/2018/09/06/Java%E5%86%85%E5%AD%98%E6%A8%A1%E5%9E%8B%E5%BC%80%E7%AF%87/",false))
		.append(dingDingNotifyMessageMarkdown.createImageOrLink("图片","" +
				"http://www.ideabuffer.cn/2017/05/06/Java%E5%AF%B9%E8%B1%A1%E5%86%85%E5%AD%98%E5%B8%83%E5%B1%80/ObjectMemoryLayout.png",true));
		DingDingNotifyMarkdown dingDingNotifyMarkdown=new DingDingNotifyMarkdown();
		dingDingNotifyMarkdown.setTitle(dingDingNotifyMessageMarkdown.createTitle("新更新了一篇博客[Java内存布局]",1));
		dingDingNotifyMarkdown.setText(sb.toString());
        dingDingNotifyMessageMarkdown.setMarkdown(dingDingNotifyMarkdown);
		ObjectMapper mapper = new ObjectMapper();
		DingDingNotifyAT at=new DingDingNotifyAT();
		at.setAtMobiles(Arrays.asList("17600738048"));at.setIsAtAll("true");
		dingDingNotifyMessageMarkdown.setAt(at);
		String json = mapper.writeValueAsString(dingDingNotifyMessageMarkdown);
		httpClientUtil.httpPostMethod(path, json,"application/json" , Maps.newHashMap(), Maps.newHashMap(),"UTF-8");
	}
	@Test
	public void testGttpGet() {
		HttpClientUtil httpClientUtil=HttpClientUtil.getInstance();
		httpClientUtil.httpGetMethod(path, Maps.newHashMap(), Maps.newHashMap(),"UTF-8");
	}

}

